package cursoandroid.ucu.themovieapp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cabanasn on 4/9/15.
 */
public class MovieProvider {

    public static List<Movie> getMovies() {

        List<Movie> movies = new ArrayList<>();

        Movie movie = new Movie();
        movie.setId(1);
        movie.setTitle("FAST & FURIOUS 6");
        movie.setDescription("Hobbs has Dominic and Brian reassemble their crew to take down a team of mercenaries: Dominic unexpectedly gets convoluted also facing his presumed deceased girlfriend, Letty.");
        movie.setCast("Dwayne Johnson, Vin Diesel, Paul Walker.");
        movie.setReleaseDate("24 May 2013");
        movie.setImageResourceId(R.drawable.fast_and_furious);
        movies.add(movie);

        movie = new Movie();
        movie.setId(3);
        movie.setTitle("HOME");
        movie.setDescription("Cuando Oh, un adorable inadaptado ser de otro mundo aterriza en la Tierra y tiene que huir de su misma especie, forma una amistad poco común con una aventurera niña llamada Tip, quien también está en una misión propia. Después de una serie de divertidas aventuras con Tip, Oh aprenderá que ser diferente y cometer errores es parte de ser humano y juntos descubrirán que no hay lugar como el hogar.");
        movie.setCast("Tim Johnson.");
        movie.setReleaseDate("27 Mar 2015");
        movie.setImageResourceId(R.drawable.home);
        movies.add(movie);

        movie = new Movie();
        movie.setId(4);
        movie.setTitle("INSURGENT");
        movie.setDescription("Secuela de Divergente. Beatrice Prior debe enfrentar sus demonios internos y continuar su pelea contra una poderosa alianza que amenaza con destruir la sociedad en la que vive.");
        movie.setCast("Naomi Watts, Shailene Woodley, Theo James.");
        movie.setReleaseDate("20 Mar 2015");
        movie.setImageResourceId(R.drawable.insurgente);
        movies.add(movie);

        movie = new Movie();
        movie.setId(5);
        movie.setTitle("FOCUS");
        movie.setDescription("Will Smith interpreta a Nicky, maestro de la estafa quien se ve involucrado sentimentalmente con la novata estafadora Jess (Margot Robbie). Mientras que él le enseña los trucos del oficio, ella se acerca demasiado al área de confort y él rompe abruptamente la relación. Tres años más tarde, la anterior novata, ahora convertida en una mujer fatal, aparece en Buenos Aires en el medio del circuito de carreras de autos de altas apuestas. En medio del último peligroso plan de Nicky, ella tira sus planes por la borda y pone al consumado estafador fuera de juego.");
        movie.setCast("Will Smith, Margot Robbie.");
        movie.setReleaseDate("27 Feb 2015");
        movie.setImageResourceId(R.drawable.focus);
        movies.add(movie);

        movie = new Movie();
        movie.setId(7);
        movie.setTitle("THE IMITATION GAME");
        movie.setDescription("During World War II, mathematician Alan Turing tries to crack the enigma code with help from fellow mathematicians.");
        movie.setCast("Keira Knightley, Benedict Cumberbatch.");
        movie.setReleaseDate("25 Dec 2014");
        movie.setImageResourceId(R.drawable.codigo_enigma);
        movies.add(movie);

        movie = new Movie();
        movie.setId(8);
        movie.setTitle("ANNIE");
        movie.setDescription("A foster kid, who lives with her mean foster mom, sees her life change when business tycoon and New York mayoral candidate Will Stacks makes a thinly-veiled campaign move and takes her in.");
        movie.setCast("Cameron Diaz, Jamie Foxx, Quvenzhané Wallis.");
        movie.setReleaseDate("19 Dec 2014");
        movie.setImageResourceId(R.drawable.annie);
        movies.add(movie);

        return movies;
    }
}
