package cursoandroid.ucu.themovieapp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;


public class MovieActivity extends ActionBarActivity {

    TextView movieTitle;
    TextView movieCast;
    TextView movieReleaseDate;
    TextView movieDescription;
    ImageView movieImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        int position = getIntent().getIntExtra(MainActivity.POSITION_PARAM, 0);

        Movie currentMovie = MovieProvider.getMovies().get(position);

        movieTitle = (TextView) findViewById(R.id.movie_title);
        movieCast = (TextView) findViewById(R.id.movie_cast);
        movieReleaseDate = (TextView) findViewById(R.id.movie_release);
        movieDescription = (TextView) findViewById(R.id.movie_description);
        movieImage = (ImageView) findViewById(R.id.movie_image);

        movieTitle.setText(currentMovie.getTitle());
        movieCast.setText(currentMovie.getCast());
        movieReleaseDate.setText(currentMovie.getReleaseDate());
        movieDescription.setText(currentMovie.getDescription());
        movieImage.setImageResource(currentMovie.getImageResourceId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_movie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
