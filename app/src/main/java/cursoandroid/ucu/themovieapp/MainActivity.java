package cursoandroid.ucu.themovieapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class MainActivity extends ActionBarActivity {

    List<Movie> movies;
    MovieListAdapter movieAdapter;
    GridView movieAdapterView;

    public static String POSITION_PARAM = "POSITION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        movieAdapterView = (GridView) findViewById(R.id.movies_adapter_view);

        movies = MovieProvider.getMovies();

        movieAdapter = new MovieListAdapter();
        movieAdapterView.setAdapter(movieAdapter);

        movieAdapterView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, MovieActivity.class);
                intent.putExtra(MainActivity.POSITION_PARAM, position);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class MovieListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return movies.size();
        }

        @Override
        public Object getItem(int position) {
            return movies.get(position);
        }

        @Override
        public long getItemId(int position) {
            return movies.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View movieView = convertView;
            if (movieView == null) {
                movieView = inflater.inflate(R.layout.movie_list_item, null);
            }

            ImageView movieImage = (ImageView) movieView.findViewById(R.id.movie_list_item_image);
            TextView movieTitle = (TextView) movieView.findViewById(R.id.movie_list_item_title);
            TextView movieYear = (TextView) movieView.findViewById(R.id.movie_list_item_year);

            Movie currentMovie = movies.get(position);
            movieImage.setImageResource(currentMovie.getImageResourceId());
            movieTitle.setText(currentMovie.getTitle());

            String[] releaseDateSplit = currentMovie.getReleaseDate().split(" ");
            movieYear.setText(releaseDateSplit[releaseDateSplit.length - 1]);

            return movieView;
        }
    }


}
